package it.cgmconsulting.msc8.post.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import it.cgmconsulting.msc8.post.entity.Post;
import it.cgmconsulting.msc8.post.payload.request.PostRequest;
import it.cgmconsulting.msc8.post.service.PostService;

@RestController
@Validated
public class PostController {
	
	@Autowired PostService postService;
	
	@PostMapping("/")
	public ResponseEntity<?> create(@RequestBody @Valid PostRequest postRequest){
		postService.save(new Post(postRequest.getTitle(), postRequest.getOverview(), postRequest.getContent()));
		return new ResponseEntity<String>("New post created: '"+postRequest.getTitle()+"'", HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> modify(@PathVariable @Min(1) long id, @RequestBody @Valid PostRequest postRequest){
		Optional<Post> p = postService.getPostById(id);
		if(!p.isPresent())
			return new ResponseEntity<String>("No post found: '"+postRequest.getTitle()+"'", HttpStatus.NOT_FOUND);
		
		p.get().setTitle(postRequest.getTitle());
		p.get().setOverview(postRequest.getOverview());
		p.get().setContent(postRequest.getContent());
		postService.save(p.get());
		
		return new ResponseEntity<String>("Post modified: '"+postRequest.getTitle()+"'", HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getPost(@PathVariable long id){
		Optional<Post> p = postService.getPostById(id);
		if(!p.isPresent())
			return new ResponseEntity<String>("No post found", HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<Optional<Post>>(p, HttpStatus.OK);
		
	}
	
	@GetMapping("/")
	public ResponseEntity<?> getPosts(){
		return new ResponseEntity<List<Post>>(postService.getAll(), HttpStatus.OK);
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable long id){
		Optional<Post> p = postService.getPostById(id);
		if(!p.isPresent())
			return new ResponseEntity<String>("No post found", HttpStatus.NOT_FOUND);
		
		postService.delete(p.get());
		return new ResponseEntity<String>("Post with id "+id+" has been deleted", HttpStatus.OK);
	}
	
	@GetMapping("/exists/{id}")
	public ResponseEntity<Boolean> getExistPost(@PathVariable long id){
		boolean b = postService.existsById(id);
		if(b)
			return new ResponseEntity<Boolean>(b, HttpStatus.OK);
		
		return new ResponseEntity<Boolean>(b, HttpStatus.NOT_FOUND);
	}
}
