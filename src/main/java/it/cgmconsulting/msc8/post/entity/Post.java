package it.cgmconsulting.msc8.post.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import it.cgmconsulting.msc8.post.entity.dateAudit.DateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @NoArgsConstructor
public class Post extends DateAudit{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(length=60, nullable=false)
	private String title;
	
	private String overview;
	
	@Column(columnDefinition="TEXT", nullable=false) //64Kb
	private String content;

	public Post(String title, String overview, String content) {
		super();
		this.title = title;
		this.overview = overview;
		this.content = content;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Post))
			return false;
		Post other = (Post) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
