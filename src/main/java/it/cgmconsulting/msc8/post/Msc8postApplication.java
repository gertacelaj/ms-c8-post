package it.cgmconsulting.msc8.post;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsC8PostApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsC8PostApplication.class, args);
	}

}
