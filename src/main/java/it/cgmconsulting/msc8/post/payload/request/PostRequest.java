package it.cgmconsulting.msc8.post.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;

@Getter
public class PostRequest {
	
	@NotBlank @Size(min=1, max=60)
	private String title;
	
	private String overview;
	
	@NotBlank @Size(min=10, max=65535)
	private String content;

}
