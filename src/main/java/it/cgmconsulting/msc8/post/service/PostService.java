package it.cgmconsulting.msc8.post.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.cgmconsulting.msc8.post.entity.Post;
import it.cgmconsulting.msc8.post.repository.PostRepository;

@Service
public class PostService {
	
	@Autowired PostRepository postRepository;
	
	public Optional<Post> getPostById(long id){
		return postRepository.findById(id);
	}
	
	public List<Post> getAll(){
		return postRepository.findAll();
	}
	
	public Post save(Post p) {
		return postRepository.save(p);
	}
	
	public void delete(Post p) {
		postRepository.delete(p);
	}
	
	public boolean existsById(long id) {
		return postRepository.existsById(id);
	}


}
